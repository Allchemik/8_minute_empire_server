//============================================================================
// Name        : websocketServer_Test.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <ctime>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

#include "Game/EmpireGame.hpp"
#include "Game/Player.hpp"
#include "WebServer/GameServer.hpp"

int main(int argc, char* argv[])
{
  try
  {
    boost::asio::io_service io_service;

    using namespace std; // For atoi.
    GameServer<EmpireGame,Player> s(io_service, 8001);//atoi(argv[1]));
    io_service.run();
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }
  return 0;
}




/*
using boost::asio::ip::tcp;

class session
{
public:
  session(boost::asio::io_service& io_service)
    : socket_(io_service)
  {
  }

  tcp::socket& socket()
  {
    return socket_;
  }

  void start()
  {
    socket_.async_read_some(boost::asio::buffer(data_, max_length),
        boost::bind(&session::handle_read, this,
          boost::asio::placeholders::error,
          boost::asio::placeholders::bytes_transferred));
  }

private:
  void handle_read(const boost::system::error_code& error,
      size_t bytes_transferred)
  {
    if (!error)
    {
      boost::asio::async_write(socket_,
          boost::asio::buffer(data_, bytes_transferred),
          boost::bind(&session::handle_write, this,
            boost::asio::placeholders::error));
    }
    else
    {
      delete this;
    }
  }

  void handle_write(const boost::system::error_code& error)
  {
    if (!error)
    {
    	boost::asio::async_read(socket_,boost::asio::buffer(data_,max_length), boost::bind(&session::handle_read, this,_1,_2));
      //socket_.async_read_some(boost::asio::buffer(data_, max_length),
      //    boost::bind(&session::handle_read, this,
      //  		  _1,_2));
           // boost::asio::placeholders::error,
         //   boost::asio::placeholders::bytes_transferred));
    }
    else
    {
      delete this;
    }
  }

  tcp::socket socket_;
  enum { max_length = 1024 };
  char data_[max_length];
};

class server
{
public:
  server(boost::asio::io_service& io_service, short port)
    : io_service_(io_service),
      acceptor_(io_service, tcp::endpoint(tcp::v4(), port))
  {
    start_accept();
  }

private:
  void start_accept()
  {
    session* new_session = new session(io_service_);
    acceptor_.async_accept(new_session->socket(),
        boost::bind(&server::handle_accept, this, new_session,boost::asio::placeholders::error));
  }

  void handle_accept(session* new_session,
      const boost::system::error_code& error)
  {
    if (!error)
    {
      new_session->start();
    }
    else
    {
      delete new_session;
    }

    start_accept();
  }

  boost::asio::io_service& io_service_;
  tcp::acceptor acceptor_;
};

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Usage: async_tcp_echo_server <port>\n";
      return 1;
    }

    boost::asio::io_service io_service;

    using namespace std; // For atoi.
    server s(io_service, atoi(argv[1]));

    io_service.run();
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}
*/

/*

using namespace boost;
//using json = nlohmann::json;

#define MEM_FN(x)		boost::bind(&self_type::x,shared_from_this())
#define MEM_FN1(x,y)	boost::bind(&self_type::x,shared_from_this(),y)
#define MEM_FN2(x,y,z)	boost::bind(&self_type::x,shared_from_this(),y,z)


asio::io_service service;

typedef boost::system::error_code error_code;
typedef shared_ptr<asio::ip::tcp::socket> socket_ptr;

asio::ip::tcp::acceptor acceptor(service,asio::ip::tcp::endpoint(asio::ip::tcp::v4(),8001));

class talk_to_client :public boost::enable_shared_from_this<talk_to_client>, boost::noncopyable
{
	typedef talk_to_client self_type;
	talk_to_client():_sock(service),_started(false){}

public:

	typedef boost::shared_ptr<talk_to_client> ptr;

	void start()
	{
		_started=true;
		do_read();
	}
	static ptr new_()
	{
		ptr new_(new talk_to_client);
		return new_;
	}
	void stop()
	{
		if(!_started)
			return;
		_started = false;
		_sock.close();
	}
	asio::ip::tcp::socket & sock()
	{
		return _sock;
	}

	void do_read()
		{
			//asio::async_read(_sock,asio::buffer(_read_buffer),MEM_FN2(read_complete,_1,_2),MEM_FN2(on_read,_1,_2));
			_sock.async_read_some(asio::buffer(_read_buffer,this->max_length),boost::bind(&on_read,this, boost::asio::placeholders::error,
			          boost::asio::placeholders::bytes_transferred));
		}
	void on_read(const error_code & err, size_t bytes)
	{
		if(!err)
		{
			std::string msg(_read_buffer, bytes);
			//std::cout<<"on_read: "<<msg<<std::endl;
			do_write(msg+"\n");
		}
		else
		{
			std::cout<<"on_read error: "<<err.message()<<std::endl;
		}
		stop();
	}
	void on_write(const error_code & err,size_t bytes)
	{
		if(!err)
			do_read();
		else
			std::cout<<"write error: "<<err.message()<<std::endl;

	}

	void do_write(const std::string & msg )
	{
		if ( !_started )
			return;
		std::copy(msg.begin(), msg.end(), _write_buffer);
		asio::async_write(_sock,asio::buffer(_write_buffer, msg.size()),MEM_FN2(on_write,asio::placeholders::error,msg.size()));
		std::cout<<"write: "<<msg<<std::endl;
	}
	size_t read_complete( const error_code & err, size_t bytes)
	{
		if ( err)
		{
			return 0;
		}
		bool found = std::find(_read_buffer, _read_buffer + bytes, '\n') < _read_buffer + bytes;
		return found ? 0: 1;
	}

private:
	//socket_ptr _sock;
	asio::ip::tcp::socket _sock;
	bool _started;
	enum {max_length= 1024};
	char _read_buffer[max_length];
	char _write_buffer[max_length];
};

void handle_accept(talk_to_client::ptr client,const error_code & err)
{

	std::cout<<"connection accpet"<<std::endl;
	client->start();
	talk_to_client::ptr new_client = talk_to_client::new_();

	acceptor.async_accept(client->sock(),boost::bind(handle_accept,new_client, asio::placeholders::error));
}

int main()
{

	talk_to_client::ptr client = talk_to_client::new_();
	acceptor.async_accept(client->sock(),boost::bind(handle_accept,client,_1));
	service.run();

    std::cout<<"program end"<<std::endl;
}
*/
/*
size_t read_complete(char *buf, const error_code &err,size_t bytes)
{
	if(err)return 0;
	bool found=std::find(buf,buf+bytes,'\n')<buf+bytes;
	return found?0:1;
}

void handle_connections()
{
	asio::ip::tcp::acceptor acceptor(service,asio::ip::tcp::endpoint(asio::ip::tcp::v4(),8001));
	char buff[1024];
	while(true){
		asio::ip::tcp::socket sock(service);
		acceptor.accept(sock);
		int bytes = asio::read(sock,asio::buffer(buff),boost::bind(read_complete,buff,_1,_2));
		std::string msg(buff,bytes);
		std::cout<<msg<<std::endl;
		sock.write_some(asio::buffer(msg));
		sock.close();
	}
}

int main()
{
	handle_connections();
}
*/
