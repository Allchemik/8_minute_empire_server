/*
 * EmpireGame.cpp
 *
 *  Created on: 17 kwi 2017
 *      Author: user
 */

#include "EmpireGame.hpp"

EmpireGame::EmpireGame(boost::shared_ptr<Client> owner,int id,int players, int mapa,std::string name):_Owner(owner),_Id(id),_maxPlayers(players),_mapaVer(mapa),_name(name)
{
	_state = state::waitingPlayers;
	if(_Players.size()<_maxPlayers)
	{
		_Players.insert(std::pair<int,boost::shared_ptr<Client>>(_Owner->getId(),_Owner));
		//std::cout<<"gracz: "<<_Owner->getId()<<" dolaczyl do gry: "<< _Id<<std::endl;
		_Owner->addtoQueue(GetPreparationGameInfo());
	}
	else
		_Owner->read();
	//addPlayer(_Owner);
}

EmpireGame::~EmpireGame() {
	//std::cout<<"destrktor gry: "<<_Id<<std::endl;
	// TODO Auto-generated destructor stub
}

void EmpireGame::addPlayer(boost::shared_ptr<Client> client)
{
	if(_Players.size()<_maxPlayers)
	{
		_Players.insert(std::pair<int,boost::shared_ptr<Client>>(client->getId(),client));
		//std::cout<<"gracz: "<<client->getId()<<" dolaczyl do gry: "<< _Id<<std::endl;
	}
	else
		client->read();
	sendPreparationGameInfo();
}
void EmpireGame::removePlayer(boost::shared_ptr<Client> client)
{
	_Players.erase(client->getId());
}
void EmpireGame::setName(std::string nazwa)
{
	_name=nazwa;
}
std::string EmpireGame::getName()
{
	return _name;
}
int EmpireGame::getId()
{
	return _Id;
}
void EmpireGame::setId(int id)
{
	_Id=id;
}
int EmpireGame::getState()
{
	return _state;
}
void EmpireGame::setState(int state)
{
	_state = state;
}
void EmpireGame::sendPreparationGameInfo()
{
	json jwrite;
	jwrite["Type"]="GameInfo";
	jwrite["data"]=getGameInfo();
	addToPlayerQueue(jwrite);
	sendQueueToPlayers();
	//sendToPlayers(jwrite);
}
json EmpireGame::GetPreparationGameInfo()
{
	json jwrite;
	jwrite["Type"]="GameInfo";
	jwrite["data"]=getGameInfo();
	return jwrite;
}
void EmpireGame::sendToPlayers(json j)
{
	for(auto it :this->_Players)
	{
		it.second->sendJson(j,false);
	}
}

void EmpireGame::addToPlayerQueue(json j)
{
	for(auto it :_Players)
	{
		_Players[it.first]->addtoQueue(j);
	}
}

void EmpireGame::sendQueueToPlayers()
{
	for(auto it :this->_Players)
	{
		it.second->sendQueue(false);
	}
}

json EmpireGame::getGameInfo()
{
	json j;
	j["GameId"]=_Id;
	j["owner"]=this->_Owner->getId();
	j["gracze"] = getPlayers();
	return j;
}

json EmpireGame::getPlayers()
{
	json j;
	for(auto it:_Players)
	{
		json l;
		l["Name"]=it.second->getName();
		l["ready"]=it.second->isReady();
		l["Id"]=it.second->getId();
		j+=l;
	}
	return j;
}
Card* EmpireGame::buyCard(boost::shared_ptr<Player> gracz,int position)
{
	Card *card=this->_Wybor[position];

	switch(position)
	{
	case 0:
		gracz->buyCard(0,card);
		break;
	case 1:
	case 2:
		gracz->buyCard(1,card);
		break;
	case 3:
	case 4:
		gracz->buyCard(2,card);
		break;
	case 5:
		gracz->buyCard(3,card);
		break;
	}

	this->_Wybor.erase(this->_Wybor.begin()+position);
	this->_Wybor.push_back(_talia.getCard());
	return card;
}
void EmpireGame::execute(json jin)
{
	switch(_state)
	{
		case state::newGame:
			setState(state::waitingPlayers);
		case state::waitingPlayers:
			executePreparation(jin);
			break;
		case state::ready:
			if(jin["Type"]=="StartGame")
			{
				InitGame();
				startGame();
			}
			else
				for(auto it :this->_Players)
				{
					it.second->read();
				}
			break;
		case state::started:
			if(jin["Type"]=="GameStarted")
			{
				int id = jin["sId"].get<int>();
				//std::cout<<"tutaj "<<id<<std::endl;
				if(responseReceived(id)==true)
				{
					_received.clear();
					addToPlayerQueue(getMapShape());
					//std::cout<<"dodano mapshape"<<std::endl;
					addToPlayerQueue(get6Cards());
					//std::cout<<"dodano 6 kart"<<std::endl;
					addToPlayerQueue(getMapState());
					//std::cout<<"dodano mapstate"<<std::endl;
					addToPlayerQueue(getPlayersState());
					json jwrite;
					jwrite["Type"]= "Lic";
					jwrite["data"]= "?";
					addToPlayerQueue(jwrite);
					//std::cout<<"dodano licytacje"<<std::endl;
					setState(Send_Bet);
					sendQueueToPlayers();
				}
			}
			break;
		case state::Send_Bet:
			if(jin["Type"]=="Bet")
			{
				int id = jin["sId"].get<int>();
				int val = std::stoi(jin["data"].get<std::string>().c_str());
				_kolejnosc.push_back(std::pair<int,int>(id,val));
				//std::cout<<" gra: "<<_Id<<"player: "<<id<<" "<<val<<std::endl;
				if(responseReceived(id)==true)
				{
					std::sort(_kolejnosc.begin(),_kolejnosc.end(),[](std::pair<int,int> v1,std::pair<int,int> v2) { return (v1.second<v2.second);});
					int max =findMax();
					//std::cout<<"max: "<<max<<" count "<<countMax(max)<<std::endl;
					if(countMax(max)>1)
					{
						_kolejnosc.clear();
						_received.clear();
						json jwrite;
						jwrite["Type"]= "Lic";
						jwrite["data"]= "?";
						sendToPlayers(jwrite);
					}
					else
					{
						_received.clear();
						setState(Game_Ongoing);
						_activePlayer=0;
						addToPlayerQueue(getMapState());
						addToPlayerQueue(get6Cards());
						sendQueueToPlayers();
						//sendToPlayers(getMapState());
						//sendToPlayers(get6Cards());
					}
				}
			}
			else
			{

			}
			break;
		case state::Game_Ongoing:
			executeGame(jin);
			break;
		case state::finished:
			// wroc gracza do lobby
			// usun gre;
			break;
	}
}
bool EmpireGame::responseReceived(int id)
{
	if(_received.find(id)==_received.end())
		_received.insert(std::pair<int,bool>(id,true));
	return (_received.size()<_maxPlayers)?false:true;
}

void EmpireGame::executeGame(json jin)
{
	if(jin["Type"]=="Card")
	{
		int id = jin["sId"].get<int>();
		if(id==getActivePlayer())
		{
			int val =  jin["data"].get<int>();
			//std::cout<<" wybrano karte "<<val<<std::endl;
			Card* card =buyCard(_Gracze[id],val);

			if(card->_akcja.size()>2)
			{
				if(card->_akcja[2]=='/')
				{
					//std::cout<<"or"<<std::endl;
					json jwrite;
					jwrite["Type"]= "Choice";
					jwrite["data"]= "?";
					//_Players[id]->sendJson(jwrite,false);
					_Players[id]->addtoQueue(jwrite);
				}
				else if(card->_akcja[2]=='+')
				{
					//std::cout<<"znalazlo pluse"<<std::endl;
					_Gracze[id]->decodeCard(0);
					_Gracze[id]->executeCard(true);
				}
			}
			else
			{
				_Gracze[id]->decodeCard(0);
				_Gracze[id]->executeCard(true);
			}
		}
	}
	else if(jin["Type"]=="Choice")
	{
		int id = jin["sId"].get<int>();
		if(id==getActivePlayer())
		{
			if(jin["data"].get<int>()==1)
			{
				_startExecuting =3;
			}
			else
			{
				_startExecuting = 0;
			}
		}
		_Gracze[id]->decodeCard(_startExecuting);
		_Gracze[id]->executeCard(true);
	}
	else
	{
		int id = jin["sId"].get<int>();
		if(id==getActivePlayer())
		{
			//std::cout<<"active player execute"<<std::endl;
			if(_Gracze[id]->executeCard(updateGame(jin)))
			{
				if(isGameEnd())
				{
					json jwrite;
					auto punkty =getWinner();
					json j,k;
					for(auto it:punkty)
					{
						j["id"]=it.first;
						j["pts"]=it.second;
						k+=j;
					}
					jwrite["Type"]="end";
					jwrite["data"]=k;
					//std::cout<<"id: "<<id<<" "<<jwrite<<std::endl;
					addToPlayerQueue(getMapState());
					addToPlayerQueue(getPlayersState());
					addToPlayerQueue(jwrite);
					//clearGame();
					//sendToPlayers(jwrite);
				}
				else
				{
					changeActivePlayer();
					addToPlayerQueue(getMapState());
					addToPlayerQueue(getPlayersState());
					addToPlayerQueue(get6Cards());
				}
			}
			else
			{
				addToPlayerQueue(getMapState());
				addToPlayerQueue(getPlayersState());
			}
		}
	}
	sendQueueToPlayers();
}

void EmpireGame::executePreparation(json jin)
{
	////std::cout<<"gra Id: "<<_Id<<" "<<jin<<std::endl;
	if(jin["Type"]=="Ready")
	{
		int id = jin["sId"].get<int>();
		_Players[id]->setReady(!_Players[id]->getRead());
		isGameReady();
		sendPreparationGameInfo();
	}
	else
	{
		for(auto it :this->_Players)
		{
			it.second->read();
		}
	}
}

void EmpireGame::startGame()
{
	json j;
	setState(state::started);
	j["Type"]="startNewGame";
	j["data"]["gracze"]=_maxPlayers;
	j["data"]["mapa"]=_mapaVer;
	sendToPlayers(j);
}

bool EmpireGame::isGameReady()
{
	if(_Players.size()<_maxPlayers)
		return false;
	for(auto it:_Players)
	{
		if(!it.second->isReady())
		{
			return false;
		}
	}
	setState(state::ready);
	return true;
}

void EmpireGame::InitGame()
{
	for(auto it:_Players)
	{
		//std::cout<<"tworzenie gracza "<<it.first<<std::endl;
		_Gracze.insert(std::pair<int,boost::shared_ptr<Player>>(it.first,boost::shared_ptr<Player>(new Player(_Players[it.first]))));
		_Players[it.first]->attachPlayer(_Gracze.at(it.first));
	}

/*
 * przygotuj talie i karty
 */
	this->_talia.loadCards("karty.txt");
	this->_talia.shuffle();
	for(int i=0;i<6;++i)
	{
		this->_Wybor.push_back(_talia.getCard());
	}
/*
 * przygotuj mape
 */
	this->_mapa.buildMap("mapa1.txt");
/*
 * prepare players
 */
	for(auto it:this->_Gracze)
	{
		//ustaw zloto na 14

		switch(_maxPlayers)
		{
		case 2:
			this->_Gracze[it.first]->setGold(14);
			_maxCards=13;
			break;
		case 3:
			this->_Gracze[it.first]->setGold(11);
			_maxCards=10;
			break;
		case 4:
			this->_Gracze[it.first]->setGold(9);
			_maxCards=8;
			break;
		case 5:
			this->_Gracze[it.first]->setGold(7);
			_maxCards=7;
			break;
		}
		//kolor jest niewazny, gracz zawsze ma czerwony, wrogowie odpowiednio inne
	}

	//wstaw do stolicy po 3 armie gracza
	for(auto it:this->_Gracze)
	{
		this->_Gracze[it.second->getId()]->addArmies(-3);
		this->_mapa.addArmy(0,it.second->getId(),3);
	}
	this->_state = Game_Prepared;
}

json EmpireGame::get6Cards()
{
	json j;
	json karty;
	j["Type"]="6cards";

	for (int i=0;i<this->_Wybor.size();++i)
	{
		karty[std::to_string(i)] +=this->_Wybor[i]->_id;
	}
	j["data"]=karty;
	j["data"]["Active"]=getActivePlayer();
	if(getActivePlayer()!=-1)
	{
		j["data"]["Max"]=_maxCards;
		j["data"]["poz"]=_Gracze[getActivePlayer()]->countCards();
	}
	return j;
}
json EmpireGame::getMapState()
{
	json j;
	j["Type"]="mapState";

	j["data"] = this->_mapa.getMapState();
	return j;
}
json EmpireGame::getMapShape()
{
	json j;
	j["Type"]="mapShape";

	j["data"] = this->_mapa.toJson();
	return j;
}

json EmpireGame::getPlayersState()
{
	json j;
	j["Type"]= "Gracze";
	for(auto it:this->_Gracze)
	{
		j["data"]+= it.second->toJson();
	}
	return j;
}

int EmpireGame::findMax()
{
	int max = -1;
	for(int i=0;i<_kolejnosc.size();++i)
	{
		if(_kolejnosc.at(i).second>max)
		{
			max = _kolejnosc.at(i).second;
		}
	}
	return max;
}

int EmpireGame::countMax(int max)
{
	int count=0;
	for(int i=0;i<_kolejnosc.size();++i)
	{
		if(_kolejnosc.at(i).second==max)
		{
			count++;
		}
	}
	return count;
}

int EmpireGame::getActivePlayer()
{
	return _activePlayer==-1?-1:_kolejnosc.at(this->_activePlayer).first;
}

int EmpireGame::changeActivePlayer()
{
	if((++_activePlayer)>=_maxPlayers)
		_activePlayer=0;

	return _kolejnosc.at(this->_activePlayer).first;
}

int EmpireGame::updateGame(json j)
{
	//std::cout<<"update Game execute"<<std::endl;
	if(j["Type"]=="ARMY")
	{
		int target =  j["data"].get<int>();
		if(_Gracze[getActivePlayer()]->getArmies()>0)
		{
			if(this->_mapa.addArmy(target,_Gracze[getActivePlayer()]->getId(),1)==0)
			{
				_Gracze[getActivePlayer()]->spawnArmy();
				//std::cout<<"gracz : "<<_Gracze[getActivePlayer()]->getId()<<"dodal armie"<<std::endl;
			}
			else
			{
				return failure;
			}
		}
		else
		{
			//std::cout<<"gracz : "<<_Gracze[getActivePlayer()]->getId()<<" nie ma wiecej armii"<<std::endl;
			return -2;
		}
	}
	else if(j["Type"]=="MOVE")
	{
		int source = j["source"].get<int>();
		int target =  j["target"].get<int>();

		std::vector<int> sasiad =this->_mapa.getLandBorder(source);
		if( std::find(sasiad.begin(), sasiad.end(), target) != sasiad.end())
		{
			if(this->_mapa.moveArmy(source,target,_Gracze[getActivePlayer()]->getId(),1)!=0)
			{
				return failure;
			}
		}
		else
		{
			//std::cout<<"to nie jest granica";
			return failure;
		}
	}
	else if(j["Type"]=="WATER")
	{
		int source = j["source"].get<int>();
		int target =  j["target"].get<int>();
		std::vector<int> sasiad =this->_mapa.getWaterBorder(source);
		if( std::find(sasiad.begin(), sasiad.end(), target) != sasiad.end())
		{
			if(this->_mapa.moveArmy(source,target,_Gracze[getActivePlayer()]->getId(),1)==-1)
			{
				return failure;
			}
		}
		else
		{
			std::vector<int> sasiad =this->_mapa.getLandBorder(source);
			if( std::find(sasiad.begin(), sasiad.end(), target) != sasiad.end())
			{
				this->_mapa.moveArmy(source,target,_Gracze[getActivePlayer()]->getId(),1);
			}
			else
			{
				//std::cout<<"to nie jest granica"<<std::endl;
				return failure;
			}
		}
	}
	else if(j["Type"]=="DESTROY")
	{
		int target =  j["data"]["target"].get<int>();
		int targetId = j["data"]["targetId"].get<int>();
		if(this->_mapa.removeArmy(target,targetId,1)==-1)
		{
			return failure;
		}
	}
	else if(j["Type"]=="CITY")
	{
		int target =  j["data"].get<int>();
		if(this->_mapa.addCity(target,_Gracze[getActivePlayer()]->getId())==-1)
		{
			return failure;
		}
	}
	return succes;
}

bool EmpireGame::isGameEnd()
{
	for(auto it:_Gracze)
		if(it.second->countCards()<_maxCards)
		{
			return false;
		}
	setState(state::finished);
	return true;
}
std::map<int,int> EmpireGame::getWinner()
{
	std::map<int,int>ControlPoints = this->_mapa.getControlPoints();
	std::map<int,int>DominationPoints = this->_mapa.getDominationPoints();
	std::map<int,int>cardPoints;
	std::map<int,int>Points;
	int winner =0,max=-1;

	for(auto it:this->_Gracze)
	{
		cardPoints.insert(std::pair<int,int>(it.first,it.second->countPoints()));
		Points.insert(std::pair<int,int>(it.first,0));
	}

	for(auto it:Points)
	{
		//std::cout<<"gracz : "<<it.first<<" kontrola: "<< ControlPoints[it.first]<< " dominacja: "<<DominationPoints[it.first]<<" karty: "<<cardPoints[it.first]<<std::endl;
		Points[it.first]=ControlPoints[it.first]+DominationPoints[it.first]+cardPoints[it.first];
	}

	for(auto it:Points)
	{
		if(it.second>max)
		{
			max = it.second;
			winner= it.first;
		}
	}
	return Points;
}

void EmpireGame::clearGame()
{
	//std::map<int,boost::shared_ptr<Client>>::iterator it;
	for(int i=0;i<_kolejnosc.size();++i)
	{
		//std::cout<<_kolejnosc[i].first<<" attachedclient id"<<std::endl;// _Players[_kolejnosc[i].second]->getId()<<" left "<<_Players.size()<<std::endl;
	}
	for(int i=0;i<_kolejnosc.size();++i)
	{
		//std::cout<<i<<" attachedclient id"<< _Players[_kolejnosc[i].first]->getId()<<" left "<<_Players.size()<<std::endl;
		 _Players[_kolejnosc[i].first]->detachGame();
	}
}

void EmpireGame::removePlayer(int id)
{
	//dummy;
}
