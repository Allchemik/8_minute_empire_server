/*
 * Deck.cpp
 *
 *  Created on: 13 mar 2017
 *      Author: Allchemik
 */

#include "Deck.hpp"

Deck::Deck(){

}

void Deck::loadCards(std::string path)
{
	std::fstream fs;
	std::string kartaTxt,element,akcja;
	int start,end,id,symbol,ilosc;

	fs.open(path.c_str(), std::fstream::in);
	if(fs.is_open())
	{
		while(!fs.eof())
		{
			start =0;
			fs>>kartaTxt;

			end = kartaTxt.find(';',start);
			element=kartaTxt.substr(start,end-start);
			std::istringstream(element)>>id;
			start = end+1;

			end = kartaTxt.find(';',start);
			element=kartaTxt.substr(start,end-start);
			std::istringstream(element)>>symbol;
			start = end+1;

			end = kartaTxt.find(';',start);
			element=kartaTxt.substr(start,end-start);
			std::istringstream(element)>>ilosc;
			start = end+1;

			end = kartaTxt.find(';',start);
			akcja = kartaTxt.substr(start,end-start);
			start = end+1;

			//kartaTemp = new Card(id,symbol,ilosc,akcja);
			this->_karty.insert(std::pair<int,Card>(id,Card(id,symbol,ilosc,akcja)));
		}
	}
	else
	{
		std::cout<<" unable to open file";
	}
	std::cout<<"kart "<<this->_karty.size()<<std::endl;
}
void Deck::shuffle()
{
	//load new deck;
	_talia.clear();
	std::random_device rd;
	auto engine = std::default_random_engine(rd());
	for(int i=1;i<_karty.size();++i)
	{
		this->_talia.push_back(&_karty[i]);
	}
	std::shuffle(std::begin(_talia), std::end(_talia), engine);
	//shuffle
}

Card* Deck::getCard()
{
	Card* karta = _talia[_talia.size()-1];
	_talia.pop_back();
	return karta;
}

std::string Deck::toString(int id)
{
	std::string strinigifiedCard;

	strinigifiedCard.append(std::to_string(_karty[id]._id));
	strinigifiedCard.push_back(';');
	strinigifiedCard.append(std::to_string(_karty[id]._symbol));
	strinigifiedCard.push_back(';');
	strinigifiedCard.append(std::to_string(_karty[id]._iloscSymboli));
	strinigifiedCard.push_back(';');
	strinigifiedCard.append(_karty[id]._akcja);
	strinigifiedCard.push_back(';');
	strinigifiedCard.push_back('\n');

	return strinigifiedCard;
}

std::string Deck::toString()
{
	std::string strinigifiedDeck;
	for(auto it : _karty)
	{
		strinigifiedDeck.append(std::to_string(it.second._id));
		strinigifiedDeck.push_back(';');
		strinigifiedDeck.append(std::to_string(it.second._symbol));
		strinigifiedDeck.push_back(';');
		strinigifiedDeck.append(std::to_string(it.second._iloscSymboli));
		strinigifiedDeck.push_back(';');
		strinigifiedDeck.append(it.second._akcja);
		strinigifiedDeck.push_back(';');
		strinigifiedDeck.push_back('\n');
	}
	return strinigifiedDeck;
}

void Deck::toJson(json& j,int id)
{
	j["id"]=_karty[id]._id;
	j["symbol"]=_karty[id]._symbol;
	j["ilosc"]=_karty[id]._iloscSymboli;
	j["akcja"]=_karty[id]._akcja;
}

void Deck::toJson(json& all)
{
	json j;
	std::cout<<"hel";

	for(auto it:_karty)
	{
		j.clear();
		j["id"]=it.second._id;
		j["symbol"]=it.second._symbol;
		j["ilosc"]=it.second._iloscSymboli;
		j["akcja"]=it.second._akcja;
		all+=j;
	}
}







