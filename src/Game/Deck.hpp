/*
 * Deck.hpp
 *
 *  Created on: 13 mar 2017
 *      Author: Allchemik
 */

#ifndef DECK_HPP_
#define DECK_HPP_

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <algorithm>
#include <random>
#include "json.hpp"

using json=nlohmann::json;

enum Symbol {
	kamien,
	drzewo,
	krysztal,
	marchew,
	zelazo,
	joker
};

/*
 * akcja:
 * A2 -> dodaj 2 armie
 * M2 -> rusz 2 armie
 * W2 -> rusz 2 armie przez wode
 * D1 -> zniszcz 1 armie
 * C1 -> wstaw 1 miasto
 * / -> lub
 * + -> i
 */

struct Card{
	int _id;
	int _symbol;
	int _iloscSymboli;
	std::string _akcja;
	Card(int id,int symbol,int ilosc, std::string akcja):_id(id),_symbol(symbol),_iloscSymboli(ilosc),_akcja(akcja){};
	Card():_id(0),_symbol(0),_iloscSymboli(0),_akcja(""){};
};

class Deck
{
public:
	Deck();

	void loadCards(std::string);
	void shuffle();
	Card* getCard();
	std::string toString();
	std::string toString(int);
	void toJson(json&);
	void toJson(json& ,int);


private:
	std::map<int,Card>_karty;
	std::vector<Card*> _talia;
};





#endif /* DECK_HPP_ */
