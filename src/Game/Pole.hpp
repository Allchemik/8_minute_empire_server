/*
 * Pole.hpp
 *
 *  Created on: 12 mar 2017
 *      Author: Allchemik
 */

#ifndef POLE_HPP_
#define POLE_HPP_

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "json.hpp"

#define PlayerId int
#define Armie int
#define Miasta int

using json = nlohmann::json;

class Pole
{
public:
	Pole(int,int);

	void addArmy(PlayerId,Armie);
	int addCity(PlayerId,Miasta);
	int getCities(PlayerId);
	int getOwner();
	int getId();
	int getKontynentId();
	int removeArmy(PlayerId,Armie);
	void addLandBorder(Pole*);
	void addWaterBorder(Pole*);

	std::vector <Pole*> getLandBorder();
	std::vector <Pole*> getWaterBorder();
	std::vector <Pole*> getAllBorder();

	json getState();


private:
	int _sasiedniePola=0;
	std::map<PlayerId,Armie> _ArmieGraczy;
	std::map<PlayerId,Miasta> _MiastaGraczy;
	std::vector<Pole*> _polaczenieLad;
	std::vector<Pole*> _polaczenieWoda;
	int _id;
	int _kontynentId;
};

#endif /* POLE_HPP_ */
