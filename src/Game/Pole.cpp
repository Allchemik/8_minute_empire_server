/*
 * Pole.cpp
 *
 *  Created on: 12 mar 2017
 *      Author: Allchemik
 */

#include "Pole.hpp"

Pole::Pole(int id, int kontynent):_id(id),_kontynentId(kontynent){

}

void Pole::addArmy(int playerId, int armie)
{
	if(_ArmieGraczy.find(playerId)==_ArmieGraczy.end())
	{
		_ArmieGraczy.insert(std::pair<int,int>(playerId,armie));
	}
	else
	{
		_ArmieGraczy[playerId]+=armie;
	}
}

int Pole::addCity(int playerId, int Miasto)
{
	if(_ArmieGraczy.find(playerId)!=_ArmieGraczy.end())
	{
		if(_ArmieGraczy[playerId]>0)
		{
			if(_MiastaGraczy.find(playerId)==_MiastaGraczy.end())
			{
				_MiastaGraczy.insert(std::pair<int,int>(playerId,Miasto));
			}
			else
			{
				_MiastaGraczy[playerId]+=Miasto;
			}
			return 0;
		}
		else
			return -1;
	}
	else
	{
		return -1;
	}
}

int Pole::getCities(int playerId)
{
	if(_MiastaGraczy.find(playerId)==_MiastaGraczy.end())
	{
		return 0;
	}
	else
	{
		return _MiastaGraczy[playerId];
	}
}

int Pole::removeArmy(int playerId,int armie)
{
	if(_ArmieGraczy.find(playerId)==_ArmieGraczy.end())
	{
		std::cout<<"nie mozna usunac armii, brak gracza"<<std::endl;
		return -1;
	}
	else
	{
		_ArmieGraczy[playerId]-=armie;
		return 0;
	}
}

int Pole::getOwner()
{
	int Owner;
	int remis =0;
	std::map<int,int> punkty;

	for(auto it =_ArmieGraczy.begin();it!=_ArmieGraczy.end();++it)
	{
		punkty.insert(std::pair<int,int>(it->first,it->second));
	}
	for(auto it =_MiastaGraczy.begin();it!=_MiastaGraczy.end();++it)
	{
		if(punkty.find(it->first)!=punkty.end())
		{
			punkty[it->first]+=it->second;
		}
		else
		{
			punkty.insert(std::pair<int,int>(it->first,it->second));
		}
	}

	if(punkty.size()<1)
	{
		return -1;
	}
	else
	{
		Owner = punkty.begin()->first;
		for(auto it =punkty.begin();it!=punkty.end();++it)
		{
			if(Owner!=it->first)
			{
				if(it->second>punkty[Owner])
				{
					Owner = it->first;
				}
				else if (it->second==punkty[Owner])
				{
					remis = 1;
				}
			}
		}
	}
	if(punkty[Owner]<1)
	{
		return -1;
	}
	if(remis ==1)
	{
		return -1;
	}
	return Owner;
}


void Pole::addLandBorder(Pole* sasiad)
{
	_polaczenieLad.push_back(sasiad);
	_sasiedniePola=_polaczenieLad.size()+_polaczenieWoda.size();
}

void Pole::addWaterBorder(Pole* sasiad)
{
	_polaczenieWoda.push_back(sasiad);
	_sasiedniePola=_polaczenieLad.size()+_polaczenieWoda.size();
}

std::vector <Pole*> Pole::getAllBorder()
{
	std::vector<Pole*> polaczenia(_polaczenieLad);
	polaczenia.insert( polaczenia.end(), _polaczenieWoda.begin(), _polaczenieWoda.end() );

	return polaczenia;
}

std::vector <Pole*> Pole::getLandBorder()
{
	return _polaczenieLad;
}

std::vector <Pole*> Pole::getWaterBorder()
{
	return _polaczenieWoda;
}

int Pole::getId()
{
	return _id;
}

int Pole::getKontynentId()
{
	return _kontynentId;
}

json Pole::getState()
{
	json j;
	json k;
	for(auto it : this->_ArmieGraczy)
	{
		k["id"]=std::to_string(it.first);
		k["il"]=it.second;
		j["armie"]+=k;
	}
	k.clear();
	for(auto it : this->_MiastaGraczy)
	{
		k["id"]=std::to_string(it.first);
		k["il"]=it.second;
		j["miasta"]+=k;
	}
	return j;

}
