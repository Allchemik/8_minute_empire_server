/*
 * EmpireGame.h
 *
 *  Created on: 17 kwi 2017
 *      Author: user
 */
#ifndef EMPIREGAME_H_
#define EMPIREGAME_H_

#include <iostream>
#include <vector>
#include <map>
#include <boost/shared_ptr.hpp>
#include "../WebServer/GameServer.hpp"
#include "Mapa.hpp"
#include "Deck.hpp"

#include "Player.hpp"

#include "json.hpp"

#define failure 0

#define succes 1

//class Client;
class Player;
class EmpireGame;

using json =nlohmann::json;

/*enum state{
	newGame,
	waitingPlayers,
	ready,
	started,
	Game_Prepared,//clienci narysowali plansze wysylamy karty
	Send_Bet,//karty otzymane, wyslac licytacje
	Game_Init,//wybrano pierwszego gracza -> rozpoczecie normalnej gry:
	Game_Ongoing,
	finished
};
*/

using Client = GameServer<EmpireGame,Player>::Client;

class EmpireGame :boost::noncopyable{
public:
	EmpireGame(boost::shared_ptr<Client> owner,int id,int players, int mapa,std::string name);
	virtual ~EmpireGame();
	void addPlayer(boost::shared_ptr<Client> client);
	void removePlayer(boost::shared_ptr<Client> client);
	void setName(std::string nazwa);
	std::string getName();
	int getId();
	void setId(int id);
	int getState();
	void setState(int state);
	void sendPreparationGameInfo();
	json GetPreparationGameInfo();
	void sendToPlayers(json);
	void sendQueueToPlayers();
	void addToPlayerQueue(json);
	bool isGameReady();
	void startGame();

	json getGameInfo();
	json getPlayers();
	void execute(json);
	void executeGame(json);
	void executePreparation(json);

	void removePlayer(int id);

//stare
	void InitGame();
	int Licytation();
	int addBet(int playerId,int bet);
	int getBets();
	int getActivePlayer();
	int changeActivePlayer();
	Card* buyCard(boost::shared_ptr<Player>  gracz,int position);
	int updateGame(json);
	std::map<int,int> getWinner();

	json get6Cards();
	json getMapState();
	json getMapShape();
	json getPlayersState();
	json update(json);//? czy na pewno tu?

	bool responseReceived(int id);

	int findMax();
	int countMax(int max);
	bool isGameEnd();
	void clearGame();

private:
	std::string _name;
	int _Id;
	int _state = 0;
	boost::shared_ptr<Client> _Owner;
	std::map<int,boost::shared_ptr<Client>> _Players;
	std::map<int,bool>_received;
	int _maxPlayers;
	int _maxCards;
	int _mapaVer;

// stare
	int _startExecuting=0;
	int _activePlayer=-1;
	std::vector<std::pair<int,int>>_kolejnosc;
	std::vector<Card*>_Wybor;
	std::map<int,boost::shared_ptr<Player>> _Gracze;
	Deck _talia;
	Mapa _mapa;
};

#endif /* EMPIREGAME_H_ */
