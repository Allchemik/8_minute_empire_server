/*
 * Player.cpp
 *
 *  Created on: 13 mar 2017
 *      Author: Allchemik
 */

#include "Player.hpp"

Player::Player():_id(0),_gold(0),_name("anonymous")
{

}
Player::Player(boost::shared_ptr<Client> client):_id(client->getId()),_gold(0),_name(client->getName()),_owner(client),_activeCard()
{
	std::cout<<"Player created "<<client.use_count()<<std::endl;
	//wywoluje destruktor....
	//_owner->attachPlayer(shared_from_this());
}

Player::Player(int id,int gold,std::string name):_id(id),_gold(gold),_name(name)
{
	std::cout<<"destruktor Player";
}

Player::Player(int id,std::string name):_id(id),_name(name)
{

}

std::string Player::getName()
{
	return _name;
}

void Player::setName(std::string name)
{
	_name = name;
}

int Player::getId()
{
	return _id;
}

void Player::setId(int id)
{
	this->_id=id;
}

int Player::getGold()
{
	return _gold;
}

void Player::setGold(int gold)
{
	this->_gold=gold;
}

void Player::modGold(int gold)
{
	this->_gold-=gold;
}

int Player::getArmies()
{
	return this->_freeArmies;
}
int Player::spawnArmy()
{
	this->_freeArmies--;
	return this->_freeArmies;
}
void Player::setArmies(int armies)
{
	this->_freeArmies=armies;
}

void Player::addArmies(int armies)
{
	this->_freeArmies+=armies;
}

bool Player::buyCard(int cost,Card* karta)
{
	if(cost>_gold)
		return false;
	_gold-=cost;
	this->_cards.push_back(karta);
	setActiveCard(karta);
	return true;
}

int Player::countCards()
{
	return this->_cards.size();
}

int Player::countPoints()
{
	int punkty=0;
	std::vector<std::pair<int,int>>symbole;

	symbole.push_back(std::pair<int,int>(Symbol::krysztal,0));
	symbole.push_back(std::pair<int,int>(Symbol::kamien,0));
	symbole.push_back(std::pair<int,int>(Symbol::drzewo,0));
	symbole.push_back(std::pair<int,int>(Symbol::zelazo,0));
	symbole.push_back(std::pair<int,int>(Symbol::marchew,0));
	symbole.push_back(std::pair<int,int>(Symbol::joker,0));

//policzyc symbole
	for(int i=0;i<this->_cards.size();++i)
	{
		for (int j=0;j< symbole.size();j++)
		{
			if(symbole[j].first==this->_cards.at(i)->_symbol)
			{
				symbole[j].second+=this->_cards.at(i)->_iloscSymboli;
				std::cout<<"znalazl"<<symbole[j].first<<" "<< this->_cards.at(i)->_iloscSymboli<<std::endl;
				break;
			}
		}
	}

	for(int i=0;i<symbole.size();++i)
	{
		std::cout<< "symbol: "<<symbole.at(i).first<<" "<<symbole.at(i).second<<std::endl;
		switch(symbole.at(i).first)
		{
			case Symbol::krysztal:
				if(symbole[i].second>0)
				{
					std::cout<<"krysztal"<<std::endl;
					if(symbole[5].second>0)
					{
						symbole[i].second+=symbole[5].second;
						symbole[5].second =symbole[i].second>4?(symbole[i].second)%4:0;
					}
					if(symbole[i].second>=4)
					{
						punkty+=5;
						symbole[i].second=0;
					}
					else if( symbole[i].second>=3)
					{
						punkty+=3;
						symbole[i].second=0;
					}
					else if(symbole[i].second>=2)
					{
						punkty+=2;
						symbole[i].second=0;
					}
					else if(symbole[i].second>=1)
					{
						punkty+=1;
						symbole[i].second=0;
					}
				}
			break;
			case Symbol::kamien:
			if(symbole[i].second>0)
			{
				std::cout<<"kamien"<<std::endl;
				if(symbole[5].second>0)
				{
					symbole[i].second+=symbole[5].second;
					symbole[5].second =symbole[i].second>5?(symbole[i].second)%5:0;
				}
				if(symbole[i].second>=5)
				{
					punkty+=5;
					symbole[i].second=0;
				}
				else if(symbole[i].second>=4)
				{
					punkty+=3;
					symbole[i].second=0;
				}
				else if(symbole[i].second>=3)
				{
					punkty+=2;
					symbole[i].second=0;
				}
				else if(symbole[i].second>=2)
				{
					punkty+=1;
					symbole[i].second=0;
				}
			}
			break;
			case Symbol::drzewo:
			if(symbole[i].second>0)
			{
				std::cout<<"drzewo"<<std::endl;
				if(symbole[5].second>0)
				{
					symbole[i].second+=symbole[5].second;
					symbole[5].second =symbole[i].second>6?(symbole[i].second)%6:0;
				}
				if(symbole[i].second>=6)
				{
					punkty+=5;
					symbole[i].second=0;
				}
				else if(symbole[i].second>=5)
				{
					punkty+=3;
					symbole[i].second=0;
				}
				else if(symbole[i].second>=4)
				{
					punkty+=2;
					symbole[i].second=0;
				}
				else if(symbole[i].second>=2)
				{
					punkty+=1;
					symbole[i].second=0;
				}
			}
			break;
			case Symbol::zelazo:
			if(symbole[i].second>0)
			{
				std::cout<<"zelazo"<<std::endl;
				if(symbole[5].second>0)
				{
					symbole[i].second+=symbole[5].second;
					symbole[5].second =symbole[i].second>7?(symbole[i].second)%7:0;;
				}
				if(symbole[i].second>=7)
				{
					punkty+=5;
					symbole[i].second=0;
				}
				else if(symbole[i].second>=6)
				{
					punkty+=3;
					symbole[i].second=0;
				}
				else if(symbole[i].second>=4)
				{
					punkty+=2;
					symbole[i].second=0;
				}
				else if(symbole[i].second>=2)
				{
					punkty+=1;
					symbole[i].second=0;
				}
			}
		break;
		case Symbol::marchew:
			if(symbole[i].second>0)
			{
				std::cout<<"marchew"<<std::endl;
				if(symbole[5].second>0)
				{
					symbole[i].second+=symbole[5].second;
					symbole[5].second =symbole[i].second>8?(symbole[i].second)%8:0;;
				}
				if(symbole[i].second>=8)
				{
					punkty+=5;
					symbole[i].second=0;
				}
				else if(symbole[i].second>=7)
				{
					punkty+=3;
					symbole[i].second=0;
				}
				else if(symbole[i].second>=5)
				{
					punkty+=2;
					symbole[i].second=0;
				}
				else if(symbole[i].second>=3)
				{
					punkty+=1;
					symbole[i].second=0;
				}
			}
		break;
		}
	}

	return punkty;
}

std::string Player::toStrng()
{
	std::string stringifiedPlayer;

	stringifiedPlayer.append(std::to_string(this->_id));
	stringifiedPlayer.push_back(';');
	stringifiedPlayer.append(this->_name);
	stringifiedPlayer.push_back(';');
	stringifiedPlayer.append(std::to_string(this->_gold));
	stringifiedPlayer.push_back(';');
	stringifiedPlayer.append(std::to_string(this->_freeArmies));
	stringifiedPlayer.push_back(';');

	for(auto it :this->_cards)
	{
		stringifiedPlayer.append(std::to_string(it->_id));
		stringifiedPlayer.push_back(',');
		stringifiedPlayer.append(std::to_string(it->_symbol));
		stringifiedPlayer.push_back(',');
		stringifiedPlayer.append(std::to_string(it->_iloscSymboli));
		stringifiedPlayer.push_back(',');
	}
	stringifiedPlayer=stringifiedPlayer.substr(0,stringifiedPlayer.size()-1);
	stringifiedPlayer.push_back(';');

	return stringifiedPlayer;
}

json Player::toJson()
{
	json j,k;

	j["id"]=this->_id;
	j["name"]=this->_name;
	j["gold"]=this->_gold;
	j["armies"]=this->_freeArmies;

	for (auto it:_cards)
	{
		k.clear();
		k["id"]=it->_id;
		k["symbol"]=it->_symbol;
		k["ilosc"]=it->_iloscSymboli;
		j["karty"]+=k;
	}

	return j;
}
void Player::setActiveCard(Card*card)
{
	_activeCard=card;
}
Card* Player::getActiveCard()
{
	return _activeCard;
}

void Player::decodeCard(int start)
{
	std::string akcja;
	std::cout<<"gracz id: "<<_id<<" decoding card"<<std::endl;
	for(int i=start;i<_activeCard->_akcja.size();++i)
	{
		std::cout<<_activeCard->_akcja.at(i)<<std::endl;
		switch(_activeCard->_akcja.at(i))
		{
			case '+':
			break;
			case '/':
				_executedAction=Action(akcja);
			//_executedAction._lastAction=0;
				std::cout<<"gracz id: "<<_id<<" decoded card"<<std::endl;
				return;
				break;
			default:
				int value=std::stoi(_activeCard->_akcja.substr(i+1,1));
				for(int j=0;j<value;++j)
				{
					akcja+=_activeCard->_akcja.at(i);
				}
				i++;
			break;
		}
	}
	_executedAction=Action(akcja);
	//_executedAction._lastAction=0;
	std::cout<<"gracz id: "<<_id<<" decoded card"<<std::endl;
}

bool Player::executeCard(bool moveOk)
{
	std::cout<<"gracz id: "<<_id<<" executing card"<<std::endl;

	if(!moveOk)
		_executedAction._lastAction--;
	json jwrite;
	if(_executedAction._lastAction<_executedAction._action.size())
	{
		switch(_executedAction._action.at(_executedAction._lastAction++))
		{
		case 'A':
			jwrite["Type"]= "ARMY";
			break;
		case 'M':
			jwrite["Type"]= "MOVE";
			break;
		case 'W':
			jwrite["Type"]= "WATER";
			break;
		case 'D':
			jwrite["Type"]= "DESTROY";
			break;
		case 'C':
			jwrite["Type"]= "CITY";
			break;
		}
		jwrite["data"]= "?";
		//this->_owner->sendJson(jwrite,false);
		_owner->addtoQueue(jwrite);
		return false;
	}
	else
		return true;
}

