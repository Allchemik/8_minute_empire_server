/*
 * Player.hpp
 *
 *  Created on: 13 mar 2017
 *      Author: Allchemik
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <boost/shared_ptr.hpp>

#include <json.hpp>
#include "Deck.hpp"
#include "../WebServer/GameServer.hpp"
#include "EmpireGame.hpp"
#include "Player.hpp"

class Player;
class EmpireGame;

using Client = GameServer<EmpireGame,Player>::Client;

struct Action{
	int _lastAction;
	std::string _action;
	Action():_action(""),_lastAction(0){};
	Action(std::string s):_action(s),_lastAction(0){};
};

class Player:public boost::enable_shared_from_this<Player>{
public:
	Player();
	Player(boost::shared_ptr<Client> client);
	Player(int id,int gold,std::string name);
	Player(int id,std::string name);
	std::string getName();
	void setName(std::string);
	int getGold();
	void setGold(int);
	void modGold(int);
	int getArmies();
	int spawnArmy();
	void setArmies(int);
	void addArmies(int);
	int getId();
	void setId(int);
	int countPoints();
	bool buyCard(int,Card*);
	int countCards();
	std::string toStrng();
	json toJson();
	void setActiveCard(Card*card);
	Card* getActiveCard();
	void decodeCard(int start);
	bool executeCard(bool);

private:
	int _id;
	int _freeArmies= 15;
	int _gold;
	int _points=0;
	std::string _name;
	std::vector<Card*> _cards;
	boost::shared_ptr<Client> _owner;
	Card* _activeCard;
	Action _executedAction;
};



#endif /* PLAYER_HPP_ */
