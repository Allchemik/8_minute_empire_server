/*
 * Mapa.hpp
 *
 *  Created on: 12 mar 2017
 *      Author: Allchemik
 */

#ifndef MAPA_HPP_
#define MAPA_HPP_

#include "Pole.hpp"

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

#include "json.hpp"

using json = nlohmann::json;

class Mapa
{
public:
	Mapa();

	void buildMap();
	void buildMap(std::string);

	int addArmy(int poleId, int graczId, int armie);
	int addCity(int poleId, int graczId);
	int removeArmy(int poleId,int graczId, int armie);
	int moveArmy(int poleId, int poleDoceloweId, int graczId, int armie);

	std::vector<int> getLandBorder(int poleId);
	std::vector<int> getWaterBorder(int poleId);

	std::map<int,int> getControlPoints();
	std::map<int,int> getDominationPoints();

	std::map<int,int> getPoints();
	std::string toString();
	json toJson();
	json getMapState();

protected:
	void dodajStolice();
	void dodajPole(int poleId,int kontynentId);
	void dodajSasiadaPoLadzie(int poleId,int sasiadId);
	void dodajSasiadaPoWadzie(int poleId,int sasiadId);

private:
	std::map<int,std::map<int,Pole*>> _Kontynenty;
	std::map<int,Pole*> _pola;

};

#endif /* MAPA_HPP_ */
