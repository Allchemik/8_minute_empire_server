/*
 * Mapa.cpp
 *
 *  Created on: 12 mar 2017
 *      Author: Allchemik
 */
#include "Mapa.hpp"

#define success 0
#define failure -1

Mapa::Mapa()
{

}

void Mapa::buildMap()
{

}

void Mapa::buildMap(std::string path)
{
	std::string mapaTxt,element;
	std::string sasiedziwoda,sasiedzilad;
	std::fstream fs;
	std::map<int,std::vector<int>> listaSasiadowLad;
	std::map<int,std::vector<int>> listaSasiadowWoda;
	int start,end,startsub,endsub;
	int tempId,kontynentId,poleId;
	fs.open(path.c_str(), std::fstream::in);
	if(fs.is_open())
	{
		while(!fs.eof())
		{
			start=0;

			fs>>mapaTxt;
			end =mapaTxt.find(';',start);
			element = mapaTxt.substr(start,end-start);
			std::istringstream(element)>>kontynentId;
			start = end+1;

			end =mapaTxt.find(';',start);
			element = mapaTxt.substr(start,end-start);
			std::istringstream(element)>>poleId;
			start = end+1;

			if(kontynentId==0)
			{
				if(poleId==0)
				{
					this->dodajStolice();
				}
				else
				{
					this->dodajPole(poleId,kontynentId);
				}
			}
			else
			{
				this->dodajPole(poleId,kontynentId);
			}
			listaSasiadowLad.insert(std::pair<int,std::vector<int>>(poleId,std::vector<int>()));
			listaSasiadowWoda.insert(std::pair<int,std::vector<int>>(poleId,std::vector<int>()));

			end =mapaTxt.find(';',start);
			sasiedzilad = mapaTxt.substr(start,end-start);
			startsub=0;
			do
			{
				endsub=sasiedzilad.find(',',startsub);
				element = sasiedzilad.substr(startsub,endsub-startsub);
				if(element.size()>0)
				{
					std::istringstream(element)>>tempId;
					listaSasiadowLad[poleId].push_back(tempId);
					startsub=endsub+1;
				}
			}
			while(endsub!=-1);
			start = end+1;

			end =mapaTxt.find(';',start);
			sasiedziwoda = mapaTxt.substr(start,end-start);
			startsub=0;
			do
			{
				endsub=sasiedziwoda.find(',',startsub);
				element = sasiedziwoda.substr(startsub,endsub-startsub);
				if(element.size()>0)
				{
					std::istringstream(element)>>tempId;
					listaSasiadowWoda[poleId].push_back(tempId);
					startsub=endsub+1;
				}
			}
			while(endsub!=-1);
			start = end+1;
		}
		fs.close();

		for(auto it :listaSasiadowLad)
		{
			for(auto itt: it.second)
			{
				dodajSasiadaPoLadzie(it.first,itt);
			}
		}
		for(auto it :listaSasiadowWoda)
		{
			for(auto itt: it.second)
			{
				dodajSasiadaPoWadzie(it.first,itt);
			}
		}
	}
	else
	{
		std::cout<<"nie mozna otworzyc"<<std::endl;
	}
}

void Mapa::dodajStolice()
{
	Pole* pole = new Pole(0,0);
	_pola.insert(std::pair<int,Pole*>(0,pole));

	std::map<int,Pole*> mapaTemp;
	mapaTemp.insert(std::pair<int,Pole*>(0,pole));

	_Kontynenty.insert(std::pair<int,std::map<int,Pole*>>(0,mapaTemp));
}

void Mapa::dodajPole(int poleId,int kontynentId)
{
	Pole* pole = new Pole(poleId,kontynentId);
	_pola.insert(std::pair<int,Pole*>(poleId,pole));
	if(_Kontynenty.find(kontynentId)!=_Kontynenty.end())
	{
		_Kontynenty[kontynentId].insert(std::pair<int,Pole*>(poleId,pole));
	}
	else
	{
		std::map<int,Pole*> mapaTemp;
		mapaTemp.insert(std::pair<int,Pole*>(poleId,pole));
		_Kontynenty.insert(std::pair<int,std::map<int,Pole*>>(kontynentId,mapaTemp));
	}
}

void Mapa::dodajSasiadaPoLadzie(int poleId,int sasiadId)
{
	_pola[poleId]->addLandBorder(_pola[sasiadId]);
}

void Mapa::dodajSasiadaPoWadzie(int poleId, int sasiadId)
{
	 _pola[poleId]->addWaterBorder(_pola[sasiadId]);
}

int Mapa::addArmy(int poleId, int graczId, int armie)
{
	if(poleId==0||_pola[poleId]->getCities(graczId)>0)
	{
		_pola[poleId]->addArmy(graczId, armie);
		return success;
	}
	else
	{
		return failure;
	}
}

int Mapa::addCity(int poleId,int graczId)
{
	if(_pola[poleId]->addCity(graczId,1)==success)
	{
		return success;
	}
	else
	{
		return failure;
	}
}

int Mapa::removeArmy(int poleId,int graczId, int armie)
{
	return _pola[poleId]->removeArmy(graczId,armie);
}

int Mapa::moveArmy(int poleId, int poleDoceloweId, int graczId, int armie)
{
	if(_pola[poleId]->removeArmy(graczId,armie)==success)
	{
		_pola[poleDoceloweId]->addArmy(graczId,armie);
		return success;
	}
	else
	{
		return failure;
	}
}

std::vector<int> Mapa::getLandBorder(int poleId)
{
	std::vector<int>borders;
	std::vector<Pole*> pola = _pola[poleId]->getLandBorder();
	for(auto it : pola)
	{
		borders.push_back(it->getId());
	}
	return borders;
}

std::vector<int> Mapa::getWaterBorder(int poleId)
{
	std::vector<int>borders;
	std::vector<Pole*> pola = _pola[poleId]->getWaterBorder();
	for(auto it : pola)
	{
		borders.push_back(it->getId());
	}
	return borders;
}

std::map<int,int> Mapa::getControlPoints()
{
	std::map<int,int>playerPoints;
	int owner;
	for(auto it : _pola)
	{
		owner = it.second->getOwner();
		if(owner!=-1)
		{
			if(playerPoints.find(owner)==playerPoints.end())
			{
				playerPoints.insert(std::pair<int,int>(owner,1));
			}
			else
			{
				playerPoints[owner]++;
			}
		}
	}
	return playerPoints;
}

std::map<int,int> Mapa::getDominationPoints()
{
	std::map<int,int>playerDomination;
	std::pair<int,int>Dominator;
	int owner;
	bool remis=false;

	for(auto it : _Kontynenty)
	{
		std::map<int,int>playerPoints;
		for(auto itt : it.second)
		{
			owner = itt.second->getOwner();
			if(owner!=-1)
			{
				if(playerPoints.find(owner)==playerPoints.end())
				{
					playerPoints.insert(std::pair<int,int>(owner,1));
				}
				else
				{
					playerPoints[owner]++;
				}
			}
		}
		if(playerPoints.size()>0)
		{
			Dominator = std::pair<int,int>(playerPoints.begin()->first,playerPoints.begin()->second);
			remis = false;
			for(auto itt: playerPoints)
			{
				if(itt.second>Dominator.second)
				{
					Dominator = std::pair<int,int>(itt.first,itt.second);
					remis = false;
				}
				else if(itt.second==Dominator.second)
				{
					if(itt.first!=Dominator.first)
					{
						remis= true;
					}
					else
					{
						remis = false;
					}
				}
			}
			if(!remis)
			{
				if(playerDomination.find(Dominator.first)==playerDomination.end())
				{
					playerDomination.insert(std::pair<int,int>(Dominator.first,1));
				}
				else
				{
					playerDomination[Dominator.first]+=1;
				}
			}
		}
	}
	return playerDomination;
}

std::map<int,int> Mapa::getPoints()
{
	std::map<int,int> punkty;
	std::map<int,int> punktyDominacji;

	punkty = getControlPoints();
	punktyDominacji = getDominationPoints();

	for(auto it :punktyDominacji)
	{
		punkty[it.first]+=it.second;
	}

	return punkty;
}

std::string Mapa::toString()
{
	std::string strinigifiedMap;

	for(auto it:_pola)
	{
		strinigifiedMap.append(std::to_string(it.second->getKontynentId()));
		strinigifiedMap.push_back(';');
		strinigifiedMap.append(std::to_string(it.second->getId()));
		strinigifiedMap.push_back(';');
		std::vector<Pole*> sasiedzi = it.second->getLandBorder();
		if(sasiedzi.size()>0)
		{
			for(auto itt : sasiedzi)
			{
				strinigifiedMap.append(std::to_string(itt->getId()));
				strinigifiedMap.push_back(',');
			}
		strinigifiedMap=strinigifiedMap.substr(0,strinigifiedMap.size()-1);
		}
		strinigifiedMap.push_back(';');
		sasiedzi = it.second->getWaterBorder();
		if(sasiedzi.size()>0)
		{
			for(auto itt : sasiedzi)
			{
				strinigifiedMap.append(std::to_string(itt->getId()));
				strinigifiedMap.push_back(',');
			}
			strinigifiedMap=strinigifiedMap.substr(0,strinigifiedMap.size()-1);
		}
		strinigifiedMap.push_back(';');
		strinigifiedMap.push_back('\n');
	}
	return strinigifiedMap;
}

json Mapa::toJson()
{
	json j;

	for(auto it:_pola)
	{
		json single;
		single["KontynentId"]=std::to_string(it.second->getKontynentId());
		single["PoleId"]=std::to_string(it.second->getId());
		for(auto itt: it.second->getLandBorder())
		{
			single["LandBorder"]+=itt->getId();
		}
		for(auto itt: it.second->getWaterBorder())
		{
			single["WaterBorder"]+=itt->getId();
		}
		//std::cout<<" wypisz: ";//		<<single;
		j+=single;
	}
	return j;
}

json Mapa::getMapState()
{
	json j;
	json k;
	for(auto it:_pola)
	{
		j["id"] = it.second->getId();
		j["zawartosc"] = it.second->getState();
		k+=j;
	}
	return k;
}
